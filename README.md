# swepp
Headerfile meant for C++ with most reserved keywords and basic datatypes, plus some other stuff, translated to Swedish via macros.

Compiles just fine in Clang/Clang++, and probably Visual C/C++ too, however, I've not tested the latter. Does not compile in gcc/g++ without workaround due to lack of support for åäö. 

Have fun.