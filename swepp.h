//
// Created by neboula on 12/03/19.
//

#ifndef SWEPP_SWEPP_H
#define SWEPP_SWEPP_H

// Datatypes
#define heltal int
#define lång long
#define kort short
#define flyttal float
#define dubbel double
#define tecken char
#define boolesk bool
#define tomrum void
#define sträng string

// Keywords
#define om if
#define annars else

#define gör do
#define medan while
#define för for
#define ty for
#define fortsätt continue

#define växel switch
#define fall case

#define försök try
#define fånga catch

#define kasta throw
#define ny new

#define strukt struct
#define klass class
#define uppräkning enum

#define offentlig public
#define privat private
#define skyddad protected

#define returnera return
#define bryt break

#define osignerad unsigned
#define signerad signed
#define konst const

#define statisk static
#define externt extern
#define namnrymnd namespace
#define använd using

#define radera delete

// Operators
#define eller ||
#define och &&

// ???
#define sant true
#define falskt false

#define VÄRDELÖS 0
#define värdelöspekare nullptr

#define vagnretur '\n'

// Functions
#define skrivutf printf
#define storlekav sizeof

#endif //SWEPP_SWEPP_H
